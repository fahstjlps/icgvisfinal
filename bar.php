<!doctype html>
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Job Overview - iCGVis</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
	<link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
	<script src="./assets/js/jquery-3.3.1.min.js"></script>
	<script src="./assets/js/angular.min.js"></script>
	<script src="./assets/js/angular-route.min.js"></script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <style>
        #weatherWidget .currentDesc {
        color: #ffffff!important;
    }
        .traffic-chart {
            min-height: 335px;
        }
        #flotPie1  {
            height: 150px;
        }
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;

        }

    </style>

</head>

<body ng-app="myApp">


    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.html"><i class="menu-icon fa fa-laptop"></i>Home </a>
                    </li>
                    <li class="menu-title">Overview</li><!-- /.menu-title -->
                    <li>
                        <a href="uploadFile.php"> <i class="menu-icon fa fa-file-text-o"></i>Create Job </a>
                    </li>
                    <li class="active">
                        <a href="job.html"> <i class="menu-icon fa fa-folder-open-o"></i>My Jobs </a>
                    </li>

                    <li class="menu-title">Other</li><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="menu-icon fa  fa-question-circle"></i>Help</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="index.html">Guide</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->



    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <!-- <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a> -->
                    <!-- <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="header-left">

                    <div class="user-area dropdown float-right" id="userProfile">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">

                            <a class="nav-link" href="#" id="signOut"><i class="fa fa-power-off"></i>Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- /header -->
        <!-- Header-->



        <div class="content pb-0">

            <!-- topic -->
            <div class="row">
                <div class="col col-lg-12">
                    <section class="card">
                        <div class="card-body text-secondary">
                            <h4 class="box-title">Upload File</h4>
                        </div>
                    </section>
                </div>
			</div>

			<a href="table.php"><button class="btn btn-outline-success btn-lg"><span>Table</span></button></a>
          <a href="cirVis.php"><button class="btn btn-outline-success btn-lg"><span>Circular</span></button></a>
		  <a href="bar.php"><button class="btn btn-primary btn-lg"><span>Bar chart</span></button></a>

		<div class="row">
            <div class="col-lg-12">
                <section class="card">
                    <div class="card-body">
						<nav class="nav">
							<b><a class="nav-link active" href="#">COLUMN</a></b>
							<b><a class="nav-link" href="#/bar2">PIE</a></b>
							<b><a class="nav-link" href="#/bar3">BAR</a></b>
						</nav>
						<div ng-view></div>
					</div>
				</section>
			</div>
		</div>
            <!-- End Topic -->



        </div>

        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        &copy; 2018, Senior Project
                    </div>
                    <div class="col-sm-6 text-right">
                        Information & Information Technology (ICT), PSU Hatyai Thailand
                    </div>
                </div>
            </div>
        </footer>


        <!-- end create job -->

        <!-- /.col-md-4 -->





    </div><!-- /#right-panel -->

    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
	<script src="assets/js/main.js"></script>
	<script>
		var app = angular.module("myApp", ["ngRoute"])
		var jobID = sessionStorage.getItem("jobID");
		app.controller("chartController1", function ($scope, $rootScope, $http, $httpParamSerializerJQLike) {
			$scope.init = () => {
				$http({
					url: './assets/php/bar.php',
					method: 'POST',
					data: $httpParamSerializerJQLike({ 'jobID': jobID }),
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then((res) => {
					$rootScope.chart = new CanvasJS.Chart("chartContainer", {
						exportEnabled: true,
						animationEnabled: true,
						theme: "light2", // "light1", "light2", "dark1", "dark2"
						title: {
							text: ""
						},
						title: {
							text: $rootScope.jobname
						},
						axisY: {
							title: "Reserves"
						},
						data: [{
							type: "column",
							legendMarkerColor: "grey",
							// legendText: "MMbbl = one million barrels",
							dataPoints: res.data
						}]
					});
					$rootScope.chart.render();
				})
			}
			$scope.changeName = () => {
				$rootScope.jobname = $scope.jobname
				$rootScope.chart.options.title.text = $rootScope.jobname
				$rootScope.chart.render();
			}
		})
		app.controller("chartController2", function ($scope, $rootScope, $http, $httpParamSerializerJQLike) {
			$scope.init = () => {
				$http({
					url: './assets/php/bar.php',
					method: 'POST',
					data: $httpParamSerializerJQLike({ 'jobID': jobID }),
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then((res) => {
						$rootScope.chart2 = new CanvasJS.Chart("chartContainer2", {
						theme: "light2", // "light1", "light2", "dark1", "dark2"
						exportEnabled: true,
						animationEnabled: true,
						title: {
							text: $rootScope.jobname
						},
						legend: {
							cursor: "pointer"
						},
						data: [{
							type: "pie",
							showInLegend: true,
							legendText: "{label}",
							toolTipContent: "{label}: <strong>{y}</strong>",
							indexLabel: "{label} - {y}",
							dataPoints: res.data
						}]
					});
					$rootScope.chart2.render();
				})
			}
			$scope.changeName = () => {
				$rootScope.jobname = $scope.jobname
				$rootScope.chart2.options.title.text = $rootScope.jobname
				$rootScope.chart2.render();
			}
		})
		app.controller("chartController3", function ($scope, $rootScope, $http, $httpParamSerializerJQLike) {
			$scope.init = () => {
				$http({
					url: './assets/php/bar.php',
					method: 'POST',
					data: $httpParamSerializerJQLike({ 'jobID': jobID }),
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then((res) => {
					$rootScope.chart3 = new CanvasJS.Chart("chartContainer3", {
						theme: "light2", // "light1", "light2", "dark1", "dark2"
						exportEnabled: true,
						animationEnabled: true,
						title: {
							text: $rootScope.jobname
						},
						axisX: {
							interval: 1
						},
						axisY: {
							// title: "Expenses in Billion Dollars",
							scaleBreaks: {
								type: "wavy",
							}
						},
						data: [{
							type: "bar",
							toolTipContent: "<b>{label}</b>: {y}<br>",
							dataPoints: res.data
						}]
					});
					$rootScope.chart3.render();
				})
			}
			$scope.changeName = () => {
				$rootScope.jobname = $scope.jobname
				$rootScope.chart3.options.title.text = $rootScope.jobname
				$rootScope.chart3.render();
			}
		})

		app.config(function ($locationProvider, $routeProvider) {
			$locationProvider.hashPrefix('');
			$routeProvider
				.when("/", {
					templateUrl: "./components/bar1.html",
					controller: "chartController1"
				})
				.when("/bar2", {
					templateUrl: "./components/bar2.html",
					controller: "chartController2"
				})
				.when("/bar3", {
					templateUrl: "./components/bar3.html",
					controller: "chartController3"
				})
		})
	</script>


</body>

</html>
