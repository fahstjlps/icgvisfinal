<?php
header('Access-Control-Allow-Origin: *');
header("Content-Type: applicaton/json");
define("host","localhost");
define("username","root");
define("password","");
define("dbname","project");
require_once 'Paginator.class.php';

class data{
    public $jobs ;
    public $page ;
}

if($_SERVER["REQUEST_METHOD"]=="POST"){
    $conn       = new mysqli(host,username,password,dbname);
    $limit      = ( isset( $_POST['limit'] ) ) ? $_POST['limit'] : 5;
    $page       = ( isset( $_POST['page'] ) ) ? $_POST['page'] : 1;
    $links      = ( isset( $_POST['links'] ) ) ? $_POST['links'] : 7;
    $userID     = $_POST['userID'];
    $query      = "SELECT * FROM `job` WHERE `userID` = '".$userID."'";

    $Paginator  = new Paginator( $conn, $query );
 
    $results    = $Paginator->getData( $limit , $page );

    for( $i = 0; $i < count( $results->data ); $i++ ) :
        $jobs[] = $results->data[$i];
    endfor; 

    $data = new data();
    $data->jobs = array_values($jobs);
    $data->page = $Paginator->createLinks( $links);
    print json_encode($data,JSON_FORCE_OBJECT);
}
 
?>
