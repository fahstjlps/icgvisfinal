<?php
	session_start();
	if(isset($_SESSION['jobID'])) {
  	// echo "Your session is running " . $_SESSION['jobID'];
	}

	// if(isset($_SESSION['txtSearch'])) {
  // 	// echo "Your session is running " . $_SESSION['jobID'];
	// }

	// if(isset($_SESSION['len'])) {
  // 	echo "Your session is running " . $_SESSION['len'];
	// }
	// else {
	// 	echo "no";
	// }

?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Upload File - iCGVis</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">

    <link rel="stylesheet" href="assets/css/style.css">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css"> -->
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/lib/datatable/dataTables.bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://d3js.org/d3.v4.min.js"></script>
		<script src="assets/js/jscolor.js"></script>

    <style>
		canvas {
				border: 1px solid #000;
		}

    </style>

</head>
<body>


    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
      <nav class="navbar navbar-expand-sm navbar-default">
          <div id="main-menu" class="main-menu collapse navbar-collapse">
              <ul class="nav navbar-nav">
                  <li class="active">
                      <a href="index.html"><i class="menu-icon fa fa-laptop"></i>Home </a>
                  </li>
                  <li class="menu-title">Overview</li><!-- /.menu-title -->
                  <li class="menu-item-has-children dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa  fa-file-text-o"></i>Create Job</a>
                      <ul class="sub-menu children dropdown-menu">
                          <li><i class="fa fa-table"></i><a href="ui-buttons.html">Table</a></li>
                          <li><i class="fa fa-bar-chart-o"></i><a href="ui-badges.html">Bar Chart</a></li>
                          <li><i class="fa fa-circle-o"></i><a href="ui-tabs.html">Circular Visualization</a></li>
                      </ul>
                  </li>
                  <li>
                      <a href="job.html"> <i class="menu-icon fa fa-folder-open-o"></i>My Jobs </a>
                  </li>

                  <li class="menu-title">Settings</li><!-- /.menu-title -->

                  <li class="menu-item-has-children dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa  fa-user"></i>Account</a>
                      <ul class="sub-menu children dropdown-menu">
                          <li><i class="menu-icon fa fa-gear"></i><a href="font-fontawesome.html">Account</a></li>
                          <li><i class="menu-icon fa fa-gear"></i><a href="font-themify.html">Visualization</a></li>
                          <!-- night mood, blind mode. -->
                      </ul>
                  </li>

                  <li class="menu-item-has-children dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-gear"></i>Visualization</a>
                  </li>

                  <li class="menu-title">Other</li><!-- /.menu-title -->
                  <li class="menu-item-has-children dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa  fa-question-circle"></i>Help</a>
                      <ul class="sub-menu children dropdown-menu">
                          <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Guide</a></li>
                      </ul>
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa  fa-info"></i>About</a>
                  </li>
              </ul>
          </div><!-- /.navbar-collapse -->
      </nav>
    </aside><!-- /#left-panel -->
    <!-- Left Panel -->



    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <!-- <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a> -->
                    <!-- <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                        <div class="dropdown for-notification" id="infoUser">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                                <span class="count bg-danger">3</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="notification">
                                <p class="red">You have 3 Notification</p>
                                <a class="dropdown-item media" href="#">
                                    <i class="fa fa-check"></i>
                                    <p>Server #1 overloaded.</p>
                                </a>
                                <a class="dropdown-item media" href="#">
                                    <i class="fa fa-info"></i>
                                    <p>Server #2 overloaded.</p>
                                </a>
                                <a class="dropdown-item media" href="#">
                                    <i class="fa fa-warning"></i>
                                    <p>Server #3 overloaded.</p>
                                </a>
                            </div>
                        </div>

                        <div class="dropdown for-message" id="msg">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="message" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-envelope"></i>
                                <span class="count bg-primary">4</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="message">
                                <p class="red">You have 4 Mails</p>
                                <a class="dropdown-item media" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                    <div class="message media-body">
                                        <span class="name float-left">Jonathan Smith</span>
                                        <span class="time float-right">Just now</span>
                                        <p>Hello, this is an example msg</p>
                                    </div>
                                </a>
                                <a class="dropdown-item media" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                    <div class="message media-body">
                                        <span class="name float-left">Jack Sanders</span>
                                        <span class="time float-right">5 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                    </div>
                                </a>
                                <a class="dropdown-item media" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                    <div class="message media-body">
                                        <span class="name float-left">Cheryl Wheeler</span>
                                        <span class="time float-right">10 minutes ago</span>
                                        <p>Hello, this is an example msg</p>
                                    </div>
                                </a>
                                <a class="dropdown-item media" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                    <div class="message media-body">
                                        <span class="name float-left">Rachel Santos</span>
                                        <span class="time float-right">15 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="user-area dropdown float-right" id = "userProfile">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="#"><i class="fa fa-user"></i>My Profile</a>

                            <a class="nav-link" href="#"><i class="fa fa-bell-o"></i>Notifications <span class="count">13</span></a>

                            <a class="nav-link" href="#"><i class="fa fa-cog"></i>Settings</a>

                            <a class="nav-link" href="#" id = "signOut"><i class="fa fa-power-off"></i>Logout</a>
                        </div>
                        <!-- <div class="dropdown for-notification" id = "infoUser">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-bell"></i>
                              <span class="count bg-danger">3</span>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="notification">
                              <p class="red">You have 3 Notification</p>
                              <a class="dropdown-item media" href="#">
                                  <i class="fa fa-check"></i>
                                  <p>Server #1 overloaded.</p>
                              </a>
                              <a class="dropdown-item media" href="#">
                                  <i class="fa fa-info"></i>
                                  <p>Server #2 overloaded.</p>
                              </a>
                              <a class="dropdown-item media" href="#">
                                  <i class="fa fa-warning"></i>
                                  <p>Server #3 overloaded.</p>
                              </a>
                          </div>

                        </div> -->
                        <!-- <button type="button" class="btn btn-outline-primary" id="signUp">Sign up</button>
                        <button type="button" class="btn btn-outline-success" id="signIn">Sign in</button> -->

                    </div>
                </div>
            </div>
        </header><!-- /header -->
        <!-- Header-->



        <div class="content pb-0">

          <!-- topic -->
            <div class="row">
                <div class="col col-lg-12">
                    <section class="card">
                        <div class="card-body text-secondary">
                          <h4 class="box-title">Upload File</h4>
                        </div>
                    </section>
                </div>
            </div>
          <!-- End Topic -->

					<a href="table.php"><button class="btn btn-outline-success btn-lg"><span>Table</span></button></a>
          <a href="cirVis.php"><button class="btn btn-primary btn-lg"><span>Circular</span></button></a>
          <a href="#"><button class="btn btn-outline-success btn-lg"><span>Bar chart</span></button></a>



          <div class="row">
              <div class="col-lg-12">
                  <section class="card">
                    <div class="card-body">

											<!-- content -->
											<form>

											  Text: <input id="textBox" placeholder="your text" />
											  <br>
												Description:<input id="DesBox" placeholder="your text" />
											  <br>


											</form>

											<form class="check-box">
												<input type="checkbox" name="ctype" value="region" checked id = "chkRegion" onclick="chk()"> Region<br>
												<input type="checkbox" name="ctype" value="gene" checked id = "chkGene" onclick="chk()"> Gene<br>
								  			<input type="checkbox" name="ctype" value="cds" checked id = "chkCDS" onclick="chk()"> CDS<br>
								  			<input type="checkbox" name="ctype" value="tRNA" checked id = "chkTRNA" onclick="chk()"> tRNA<br>
												<input type="checkbox" name="ctype" value="rRNA" checked id = "chkRRNA" onclick="chk()"> rRNA<br>
												<input type="checkbox" name="ctype" value="other" checked id = "chkOther" onclick="chk()"> Other<br><br>
											</form>
											<!-- <button onclick="submitBtn()">Submit</button><br /> -->
											<button class = "chageRegion">Region color</button><br />






											<p>Background color:
											<input class="jscolor {onFineChange:'update(this)'}" value="cc66ff">

											<!-- <p id="rect" style="border:1px solid gray; width:161px; height:100px;"> -->

											<div class="form-element form-submit">
													<!-- <a href="" id="export-png"  download="my-file-name.png"><button>Export</button></a> -->

													<select  name="Export" id="Export">
													<option value="">---- Export ----</option>
													<option value="export-png">PNG</option>
													<option value="export-jpg">JPG</option>
													<option value="export-bmp">BMP</option>
													</select>

													<a href="" id="exportBtn"><button>Export</button></a>
											</div>

													<canvas id="canvas"></canvas>
													<script>


															var canvas = d3.select("canvas").call(d3.zoom().scaleExtent([1, 1000]).on("zoom", zoom)),
																			context = canvas.node().getContext("2d"),
																			width = canvas.property("width"),
																			height = canvas.property("height");
															//            var canvas = document.getElementById("canvas");
															//            var context = canvas.getContext("2d");

															var canvasOffset = $("#canvas").offset();
															var offsetX = canvasOffset.left;
															var offsetY = canvasOffset.top;

															var w = window.innerWidth - 100;
															var h = window.innerHeight - 100;

															context.canvas.width = w;
															context.canvas.height = h;
															context.lineWidth = 20;
									//            context.fillStyle = "white";

															var radius = h / 3;

															var centerX = w / 2;
															var centerY = h / 2;

															var len = 1000000;
															var arcs = [];
															// var jobID=1;
															var jobID = <?php echo $_SESSION["jobID"];?>; // create variable of jobID

															// console.log(jobID);
															var inputs = document.querySelectorAll('input[type="checkbox"]');

															var button = document.querySelector('.chageRegion');

															var canvas = document.querySelector('canvas');
															var dataURL = canvas.toDataURL();

															var message = "Title";
															var message2 = "Description";
													    var fillOrStroke ="fill";

															var formElement = document.getElementById("textBox");
													    formElement.addEventListener("keyup", textBoxChanged, false);

															var formElement2 = document.getElementById("DesBox");
													    formElement2.addEventListener("keyup", textBoxChanged2, false);

															var checkBoxGene = document.getElementById("chkGene");

															console.log(dataURL);
															// "data:image/png;base64

															// console.log(inputs);

															console.log(jobID);

															// var c = canvas.getContext('2d');
															// c.fillStyle="gray";
															// c.fillRect(w-250, h-500, 20, 20);
															//
															// c.fillStyle="#feb236";
															// c.fillRect(w-250, h-470, 20, 20);
															//
															// c.fillStyle="#d64161";
															// c.fillRect(w-250, h-440, 20, 20);
															//
															// c.fillStyle="#ff7b25";
															// c.fillRect(w-250, h-410, 20, 20);
															//
															// c.fillStyle="#405d27";
															// c.fillRect(w-250, h-380, 20, 20);
															//
															// c.fillStyle="green";
															// c.fillRect(w-250, h-350, 20, 20);
															//

															$(document).ready(function () {
									//                console.log("ready");
																	importGFF();

															});


															// create some test data objects


															// $(function() {
															//     $.ajax({
															//         url: 'queryGFF.php', //the script to call to get data
															//         data: "",
															//         dataType: 'json', //data format
															//         success: function(data) //on recieve of reply
															//         {
															//             t = data[0];

															//             $.each(t, function(i, item) {
															//                 arcs.push({
															//                     start: t[i].startPos,
															//                     end: t[i].endPos,
															//                     type: t[i].gtype,
															//                     sequence: ''
															//                 });
															//                 // console.log(t[i].startPos);
															//             });
															//         }
															//     });

															// });
															// console.log(arcs);

															//outer arcs
															// arcs.push({
															//     start: 0,
															//     end: 1000000,
															//     type: 'genome',
															//     sequence: ''
															// });
															// arcs.push({
															//     start: 0,
															//     end: 2000,
															//     type: 'gene',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 6000,
															//     end: 10000,
															//     type: 'gene',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 0,
															//     end: 2000,
															//     type: 'cds',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 6000,
															//     end: 10000,
															//     type: 'cds',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 50000,
															//     end: 90000,
															//     type: 'rrna',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// // inner arcs
															// arcs.push({
															//     start: 100000,
															//     end: 120000,
															//     type: 'gene',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 100000,
															//     end: 120000,
															//     type: 'cds',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 400000,
															//     end: 450000,
															//     type: 'trna',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 810000,
															//     end: 850000,
															//     type: 'gene',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 810000,
															//     end: 850000,
															//     type: 'cds',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });

															// arcs.push({
															//     start: 610000,
															//     end: 680000,
															//     type: 'gene',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });
															// arcs.push({
															//     start: 610000,
															//     end: 680000,
															//     type: 'cds',
															//     sequence: 'ATCGGGTGGGTTTTGGGGGGGG'
															// });

															// visibly draw all arcs


									//            for (var i = 0; i < arcs.length; i++) {
									//                console.log("ready");
									//                drawCircle(arcs[i], R);
									//            }


															function importGFF()
															{
															//   console.log("ttt");
																	$.ajax({
																			url: 'assets/php/queryGFF.php', //the script to call to get data
																			type: 'post',
																			dataType:'json', // add json datatype to get json
																			data: {
																				'jobID' : jobID
																				// 'len' : len
																			},
																			success: function (data) //on recieve of reply
																			{
																					// console.log(data);
																					t = data[0];

																					$.each(t, function (i, item) {
																							arcs.push({
																									start: t[i].startPos,
																									end: t[i].endPos,
																									type: t[i].ctype,
																									sequence: ''
																							});
																							// console.log(t[i].startPos);

																					});
																					draw(d3.zoomIdentity);



																			},
																			error: function(xhr, ajaxOptions, thrownError){
																				alert(xhr.status);
																				alert(thrownError);
																			}
																	});
															}

															function draw(transform) {


																	// var checkGene2 = document.getElementById("chkRegion");
															    // var checkBox2 = document.getElementById("myCheck2");
																	var i = -1, n = arcs.length, d;

																	//
																	// if (checkGene2.checked == true) {
																	// 	context.fillRect(500, 100, 100, 100);
																	// } else {
																	// 	// c.clearRect(0, 0, canvas.width, canvas.height);
																	// }


																	// console.log(transform.x);

																// Title
																// var stringTitle = document.getElementById('title').value;
																// console.log(stringTitle);

																// context.font = "40px Georgia";
																// context.fillText("Title", transform.applyX(centerX) , transform.y -30 );
																// context.textAlign="center";
																//
																// context.font = "20px Georgia";
																// context.fillText("Description", transform.applyX(centerX) , transform.y -10);
																// context.textAlign="center";

																drawScreen();


																function drawScreen() {


																context.font = "50px serif";
																var metrics = context.measureText(message);
													      var textWidth = metrics.width;
													      var xPosition = (transform.applyX(centerX));
													      var yPosition = (transform.y -30);

																var xPosition2 = (transform.applyX(centerX));
													      var yPosition2 = (transform.y -10);

																// switch(fillOrStroke) {
													      //    case "fill":
													      context.fillStyle = "#000";
																context.textAlign="center";
													      context.fillText (message, xPosition,yPosition + 340);


																context.font = "20px serif";
																context.fillStyle = "#000";
																context.textAlign="center";
													      context.fillText (message2, xPosition2,yPosition2 + 340);


													            // break;
													      //    case "stroke":
													      //       context.strokeStyle = "#FF0000";
													      //       context.strokeText (message, xPosition,yPosition);
													      //       break;
													      //    case "both":
													      //       context.fillStyle = "#FF0000";
													      //           context.fillText (message, xPosition ,yPosition);
													      //       context.strokeStyle = "#000000";
													      //       context.strokeText (message, xPosition,yPosition);
													      //       break;
													      // }

																var c = canvas.getContext('2d');
																context.textAlign="left";

																c.fillStyle="#112F41";
																c.fillRect(w-250, h-500, 20, 20);
																c.font = "20px serif";
																c.fillStyle = "#000";
																c.fillText ("Region", w-200, h-485);


																c.fillStyle="#068587";
																c.fillRect(w-250, h-470, 20, 20);
																c.font = "20px serif";
																c.fillStyle = "#000";
																c.fillText ("Gene", w-200, h-455);

																c.fillStyle="#4FB99F";
																c.fillRect(w-250, h-440, 20, 20);
																c.font = "20px serif";
																c.fillStyle = "#000";
																c.fillText ("CDS", w-200, h-425);

																c.fillStyle="#F2B134";
																c.fillRect(w-250, h-410, 20, 20);
																c.font = "20px serif";
																c.fillStyle = "#000";
																c.fillText ("tRNA", w-200, h-395);

																c.fillStyle="#ED553B";
																c.fillRect(w-250, h-380, 20, 20);
																c.font = "20px serif";
																c.fillStyle = "#000";
																c.fillText ("rRNA", w-200, h-365);

																c.fillStyle="#EB6896";
																c.fillRect(w-250, h-350, 20, 20);
																c.font = "20px serif";
																c.fillStyle = "#000";
																c.fillText ("Other type", w-200, h-335);




															}



															draw.drawScreen = drawScreen;





																	while (++i < n) {

																			// context.clearRect(0, 0, w, h);
																			var checkRegion = document.getElementById("chkRegion");
																			var checkGene = document.getElementById("chkGene");
																			var checkCDS = document.getElementById("chkCDS");
																			var checkTRNA = document.getElementById("chkTRNA");
																			var checkRRNA = document.getElementById("chkRRNA");
																			var checkOther = document.getElementById("chkOther");

																			dx = transform.applyX(centerX);
																			dy = transform.applyY(centerY);
																			context.moveTo(dx, dy);





										                    if (arcs[i].type === 'region') {
																					if(checkRegion.checked == true){
										                        context.beginPath();
										                        context.arc(dx, dy, (radius - 20)*transform.k, getRadian(0), getRadian(360));
										                        context.strokeStyle = "#112F41";

																						context.lineWidth = 10;

										                        context.stroke();


																						button.onclick = function () {
																							var red = Math.floor(Math.random() * 256);
									 														var blue = Math.floor(Math.random() * 256);
									 														var green = Math.floor(Math.random() * 256);
																							context.beginPath();
											                        context.arc(dx, dy, (radius - 20)*transform.k, getRadian(0), getRadian(360));
																							 context.strokeStyle = "rgb(" + red + "," + green + "," + blue + ")";
																							 context.stroke();
										 	                        context.lineWidth = 20;
																						};

										                    }

																			}
																			$("#chkRegion").click(function () {
																			if (inputs[0].unchecked){
																				console.log("un");
																				 if (arcs[i].type === 'region') {
																					 // var regionPath = arcs[i].type;
																					 // regionPath.hide();
																					 context.clearRect(0, 0, canvas.width, canvas.height);
																				 }
																			}
																		});








									                     if (arcs[i].type === 'gene') {
																				 if(checkGene.checked == true){
																					 // console.log("1 chk");
									                        context.beginPath();
									                        context.arc(dx, dy, radius*transform.k, getRadian(360 * arcs[i].start / len), getRadian(360 * arcs[i].end / len));
									                        context.strokeStyle = "#068587";
									                        context.stroke();

																					function chageGene() {
																						context.strokeStyle = "#564323";
																						context.stroke();
																					}
																					// chageGene();
									                        context.lineWidth = 20;
									                    	}
																			}

																			// if (inputs[1].unchecked){
																			// 	 if (arcs[i].type === 'gene') {
																			// 		 context.clearRect(0, 0, canvas.width, canvas.height);
																			// 	 }
																			// }


																			 if (arcs[i].type === 'CDS') {
																				 if(checkCDS.checked == true){
										                        context.beginPath();
										                        context.arc(dx, dy, (radius + 20)*transform.k, getRadian(360 * arcs[i].start / len), getRadian(360 * arcs[i].end / len));
										                        context.strokeStyle = "#4FB99F";
										                        context.stroke();
										                        context.lineWidth = 20;

																						// console.log(arcs[i].start);
										                    }
																			}

																			// if (inputs[2].unchecked){
																			// 	 if (arcs[i].type === 'CDS') {
																			// 		 context.clearRect(0, 0, canvas.width, canvas.height);
																			// 	 }
																			// }

																			// if(inputs[3].checked){
																			 if (arcs[i].type === 'tRNA') {
																				 if(checkTRNA.checked == true){
										                        context.beginPath();
										                        context.arc(dx, dy, (radius + 40)*transform.k, getRadian(360 * arcs[i].start / len), getRadian(360 * arcs[i].end / len));
										                        context.strokeStyle = "#F2B134";
										                        context.stroke();
										                        context.lineWidth = 20;
																					}
										                    }
																			// }

																			// if (inputs[3].unchecked){
																			// 	 if (arcs[i].type === 'tRNA') {
																			// 		 context.clearRect(0, 0, canvas.width, canvas.height);
																			// 	 }
																			// }


																				 if (arcs[i].type === 'rRNA') {
																					 if(checkRRNA.checked == true){
										                        context.beginPath();
										                        context.arc(dx, dy, (radius + 60)*transform.k, getRadian(360 * arcs[i].start / len), getRadian(360 * arcs[i].end / len));
										                        context.strokeStyle = "#ED553B";
										                        context.stroke();
										                        context.lineWidth = 20;
																					}
										                    }


																			// if (inputs[4].unchecked){
																			// 	 if (arcs[i].type === 'rRNA') {
																			// 		 context.clearRect(0, 0, canvas.width, canvas.height);
																			// 	 }
																			// }


																			if (arcs[i].type !== 'region' && arcs[i].type !== 'gene' && arcs[i].type !== 'CDS' && arcs[i].type !== 'tRNA' && arcs[i].type !== 'rRNA') {
																					if(checkOther.checked == true){
																						context.beginPath();
										                        context.arc(dx, dy, (radius + 80)*transform.k, getRadian(360 * arcs[i].start / len), getRadian(360 * arcs[i].end / len));
										                        context.strokeStyle = "#EB6896";
										                        context.stroke();
																					}

										                    }


																			// if (inputs[5].unchecked){
																			// 	 if (arcs[i].type !== 'region' && arcs[i].type !== 'gene' && arcs[i].type !== 'CDS' && arcs[i].type !== 'tRNA' && arcs[i].type !== 'rRNA') {
																			// 		 context.clearRect(0, 0, canvas.width, canvas.height);
																			// 	 }
																			// }


																	}



															}





															// handle mousemove events

															//            function handleMouseMove(e) {
															//
															//                // get mouse position
															//                mouseX = parseInt(e.clientX - offsetX);
															//                mouseY = parseInt(e.clientY - offsetY);
															//
															//                // reset the results box to invisible
															//                context.clearRect(225 - 20, 30 - 20, 100, 100);
															//
															//                // hit-test each arc
															//                for (var i = 0; i < arcs.length; i++) {
															//
															////                    drawCircle(arcs[i],R);
															//
															//                    if (context.isPointInStroke(mouseX, mouseY)) {
															//                        context.strokeStyle = "green";
															//                        context.fillText(arcs[i].type, 225, 30);
															//                        return;
															//                    }
															//
															//                }
															//
															//            }
															function getRadian(degree) {
																	return (degree - 90) * (Math.PI / 180);
															}

															function zoom() {
																	context.clearRect(0, 0, w, h);
																	draw(d3.event.transform);
															}

															// function submitBtn() {
															// 	$("#chkPassport").click(function () {
															// 		 if ($(this).is(":checked")) {
															// 				 $("#dvPassport").show();
															// 				 $("#AddPassport").hide();
															// 		 } else {
															// 				 $("#dvPassport").hide();
															// 				 $("#AddPassport").show();
															// 		 }
															//  });
															// }

															function animate() {
																requestAnimationFrame(animate);
																console.log("fa");
																// context.clearRect(0, 0, innerWidth, innerHeight);
															}

															animate();

															function textBoxChanged(e) {
																 var target = e.target;
																 message = target.value;
																 context.clearRect(0, 0, w, h);
																 draw(d3.zoomIdentity);
    												 		 draw.drawScreen();

															}

															function textBoxChanged2(e) {
																 var target = e.target;
																 message2 = target.value;
																 context.clearRect(0, 0, w, h);
																 draw(d3.zoomIdentity);
    												 			draw.drawScreen();

															}

															function clearC() {
																console.log("cls");
																context.clearRect(0, 0, w, h);
															}

															function chk() {
																// context.clearRect(0, 0, w, h);
																var checkBox1 = document.getElementById("chkRegion");
														    var checkBox2 = document.getElementById("chkGene");
																var checkBox3 = document.getElementById("chkCDS");
																var checkBox4 = document.getElementById("chkTRNA");
																var checkBox5 = document.getElementById("chkRRNA");
																var checkBox6 = document.getElementById("chkOther");

														    // var checkBox2 = document.getElementById("myCheck2");
														    // var text = document.getElementById("text");
														    if (checkBox1.checked == true){
																	// clearC()
																	draw(d3.zoomIdentity);
																	console.log("chk");
														    } else {
														    	console.log("unchk");
																	context.clearRect(0, 0, w, h);
																	draw(d3.zoomIdentity);
														    }

																if (checkBox2.checked == true){
																	// clearC()
																	draw(d3.zoomIdentity);
																	console.log("chk");
									    					} else {
														    	console.log("unchk");
																	context.clearRect(0, 0, w, h);
																	draw(d3.zoomIdentity);
														    }

																if (checkBox3.checked == true){
																	// clearC()
																	draw(d3.zoomIdentity);
																	console.log("chk");
									    					} else {
														    	console.log("unchk");
																	context.clearRect(0, 0, w, h);
																	draw(d3.zoomIdentity);
														    }

																if (checkBox4.checked == true){
																	// clearC()
																	draw(d3.zoomIdentity);
																	console.log("chk");
									    					} else {
														    	console.log("unchk");
																	context.clearRect(0, 0, w, h);
																	draw(d3.zoomIdentity);
														    }

																if (checkBox5.checked == true){
																	// clearC()
																	draw(d3.zoomIdentity);
																	console.log("chk");
									    					} else {
														    	console.log("unchk");
																	context.clearRect(0, 0, w, h);
																	draw(d3.zoomIdentity);
														    }

																if (checkBox6.checked == true){
																	// clearC()
																	draw(d3.zoomIdentity);
																	console.log("chk");
									    					} else {
														    	console.log("unchk");
																	context.clearRect(0, 0, w, h);
																	draw(d3.zoomIdentity);
														    }

																// if (checkBox.checked == true){
																// 	draw(d3.zoomIdentity);
																// 	console.log("chk");
														    //   // c.fillRect(20, 20, 150, 100);
														    //   // c.rect(20,20,150,100);
														    //   // c.fill();
														    // }

															}






															// listen for mousemoves

															//            $("#canvas").mousemove(function (e) {
															//                handleMouseMove(e);
															//            });
															//            $(window).resize(function () {
															//                location.reload();
															//            });
													</script>
													<script>
															// $('#export-png').click(function(){
															//     var canvas = document.getElementById("canvas");
															//     var img    = canvas.toDataURL("image/png");
															//     $(this).attr('href',img);
															// });
															// $('#export-jpg').click(function(){
															//     var canvas = document.getElementById("canvas");
															//     var img    = canvas.toDataURL("image/jpg");
															//     $(this).attr('href',img);
															// });
															// $('#export-bmp').click(function(){
															//     var canvas = document.getElementById("canvas");
															//     var img    = canvas.toDataURL("image/bmp");
															//     $(this).attr('href',img);
															// });


																	//  document.getElementById("exportBtn").style.backgroundColor = myVar;

																	// console.log(exportSelected);
																	// if(exportSelected = "export-png") {
																	//     $('#exportBtn').click(function(){
																	//         var canvas = document.getElementById("canvas");
																	//         console.log(exportSelected);
																	//         var img    = canvas.toDataURL("image/png");
																	//         $(this).attr('href',img);
																	//     });
																	// }
																	// else if (exportSelected = "export-jpg") {
																	//     $('#exportBtn').click(function(){
																	//         var canvas = document.getElementById("canvas");
																	//         console.log(exportSelected);
																	//         var img    = canvas.toDataURL("image/jpg");
																	//         $(this).attr('href',img);
																	//     });
																	// }

																	// else if (exportSelected = "export-bmp") {
																	//     $('#exportBtn').click(function(){
																	//         var canvas = document.getElementById("canvas");
																	//         console.log(exportSelected);
																	//         var img    = canvas.toDataURL("image/bmp");
																	//         $(this).attr('href',img);
																	//     });
																	// }
																	// $(this).attr('href',img);

																	$('#exportBtn').click(function(){
																			context.clearRect(0, 0, w, h);
																			draw(d3.zoomIdentity);
																			var exportType = $('#Export').val();
																			switch(exportType){
																					case 'export-png' :
																							var canvas = document.getElementById("canvas");
																							var img    = canvas.toDataURL("image/png");
																							$('#exportBtn').attr('href',img);
																							$('#exportBtn').attr('download','download.png');
																							break;
																					case 'export-jpg':
																							var canvas = document.getElementById("canvas");
																							var img    = canvas.toDataURL("image/jpeg");
																							$('#exportBtn').attr('href',img);
																							$('#exportBtn').attr('download','download.jpg');
																							break;
																					case 'export-bmp':
																							var canvas = document.getElementById("canvas");
																							var img = canvas.toDataURL('image/bmp');
																							$('#exportBtn').attr('href',img);
																							$('#exportBtn').attr('download','download.bmp');
																							break;
																			}
																	})



													</script>



											<!-- end content -->


										</div>
                  </section>
              </div>




              </div>
						</div>

              <!--/.col-->






            <!-- create job -->
            <div class="row">
              <div class="col-xl-8">


              </div>
            </div>

            </div>

            <div class="clearfix"></div>

            <footer class="site-footer">
                <div class="footer-inner bg-white">
                    <div class="row">
                        <div class="col-sm-6">
                             &copy; 2018, Senior Project
                        </div>
                        <div class="col-sm-6 text-right">
                            Information & Information Technology (ICT), PSU Hatyai Thailand
                        </div>
                    </div>
                </div>
            </footer>


            <!-- end create job -->

            <!-- /.col-md-4 -->





    </div><!-- /#right-panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


		<script>
		function update(jscolor) {
		    // 'jscolor' instance can be used as a string
		    document.getElementById('canvas').style.backgroundColor = '#' + jscolor;
				// var cl = "#" + jscolor;
				// return cl;
		}
		</script>




<!-- <div id="container">



</div> -->



</body>
</html>
