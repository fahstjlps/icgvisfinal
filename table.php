<?php
	session_start();
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Upload File - iCGVis</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">


    <link href="assets/weather/css/weather-icons.css" rel="stylesheet" />
    <link href="assets/calendar/fullcalendar.css" rel="stylesheet" />

    <link rel="stylesheet" href="assets/css/style.css">
    <link href="assets/css/charts/chartist.min.css" rel="stylesheet">
    <link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <script src="assets/js/index1.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="js/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css"> -->
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://d3js.org/d3.v4.min.js"></script>


    <style>
    #weatherWidget .currentDesc {
        color: #ffffff!important;
    }
        .traffic-chart {
            min-height: 335px;
        }
        #flotPie1  {
            height: 150px;
        }
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;
        }

				#geneData {
				  border-collapse: collapse;
				  width: 100%;
				}

				#geneData td, #geneData th {
				  border: 1px solid #ddd;
				  padding: 8px;
				}

				#geneData tr:nth-child(even){background-color: #f2f2f2;}

				#geneData tr:hover {background-color: #ddd;}

				#geneData th {
				  padding-top: 12px;
				  padding-bottom: 12px;
				  text-align: left;
				  background-color: #4CAF50;
				  color: white;
				}

    </style>

</head>
<body>


	<!-- Left Panel -->
  <aside id="left-panel" class="left-panel">
      <nav class="navbar navbar-expand-sm navbar-default">
          <div id="main-menu" class="main-menu collapse navbar-collapse">
              <ul class="nav navbar-nav">
                  <li>
                      <a href="index.html"><i class="menu-icon fa fa-laptop"></i>Home </a>
                  </li>
                  <li class="menu-title">Overview</li><!-- /.menu-title -->
                  <li class="active">
                      <a href="uploadFile.php"> <i class="menu-icon fa fa-file-text-o"></i>Create Job </a>
                  </li>
                  <li>
                      <a href="job.html"> <i class="menu-icon fa fa-folder-open-o"></i>My Jobs </a>
                  </li>

                  <li class="menu-title">Other</li><!-- /.menu-title -->
                  <li class="menu-item-has-children dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="menu-icon fa  fa-question-circle"></i>Help</a>
                      <ul class="sub-menu children dropdown-menu">
                          <li><i class="menu-icon fa fa-sign-in"></i><a href="index.html">Guide</a></li>
                      </ul>
                  </li>
              </ul>
          </div><!-- /.navbar-collapse -->
      </nav>
  </aside><!-- /#left-panel -->
  <!-- Left Panel -->



  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">

      <!-- Header-->
      <header id="header" class="header">
          <div class="top-left">
              <div class="navbar-header">
                  <!-- <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a> -->
                  <!-- <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
                  <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
              </div>
          </div>
          <div class="top-right">
              <div class="header-menu">
                  <div class="header-left">

                  <div class="user-area dropdown float-right" id="userProfile">
                      <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true"
                          aria-expanded="false">
                          <img class="user-avatar rounded-circle" src="admin.jpg" alt="User Avatar">
                      </a>

                      <div class="user-menu dropdown-menu">

                          <a class="nav-link" href="#" id="signOut"><i class="fa fa-power-off"></i>Logout</a>
                      </div>
                  </div>
              </div>
          </div>
      </header><!-- /header -->
      <!-- Header-->



        <div class="content pb-0">

          <!-- topic -->
            <div class="row">
                <div class="col col-lg-12">
                    <section class="card">
                        <div class="card-body text-secondary">
                          <h4 class="box-title">Table</h4>
                        </div>
                    </section>
                </div>
            </div>
          <!-- End Topic -->

          <a href="#"><button class="btn btn-primary btn-lg"><span>Table</span></button></a>
          <a href="cirVis.php"><button class="btn btn-outline-success btn-lg"><span>Circular</span></button></a>
          <a href="bar.php"><button class="btn btn-outline-success btn-lg"><span>Bar chart</span></button></a>


          <?php

            include 'assets/php/db_connection.php';
            $con = OpenCon();

          error_reporting(E_ALL ^ E_NOTICE);

          $target_dir = "uploads/";
					$target_dir2 = "uploads2/";
          $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
					$target_file2 = $target_dir2 . basename($_FILES["fileToUpload2"]["name"]);
          $uploadOk = 1;
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                    $imageFileType2 = strtolower(pathinfo($target_file2,PATHINFO_EXTENSION));
                    
          $jobName = $_POST['jobName'];
          $userID = $_POST['userID'];

          $jobInsertSql = "INSERT INTO `job` (`Job_Name`, `userID`, `jobDate`) VALUES ('".$jobName."', '".$userID."', NOW())";
          $result = mysqli_query($con, $jobInsertSql);
          $jobID = $con->insert_id;

          $myid = 1;

          echo  "<script>
          console.log('sss')
          sessionStorage.setItem('jobID', ".$myid.");
          </script>";

          // $jobID = $_SESSION['jobID'];

          // echo $jobID;
					echo "<br />";

					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
              echo '<div class="sufee-alert alert with-close alert-primary alert-dismissible fade show">
										<span class="badge badge-pill badge-primary">success</span>
										The file '. basename( $_FILES["fileToUpload"]["name"]). ' has been uploaded.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
										</button>
										</div>
										';

          } else {
              echo '  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
										<span class="badge badge-pill badge-danger">Fail</span>
										Sorry, there was an error uploading your file.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
										</button>
										</div>';
          }

          if (move_uploaded_file($_FILES["fileToUpload2"]["tmp_name"], $target_file2)) {
              echo '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
										<span class="badge badge-pill badge-primary">success</span>
										The file '. basename( $_FILES["fileToUpload"]["name"]). ' has been uploaded.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
										</button>
										</div>
										';

          } else {
              echo '  <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
										<span class="badge badge-pill badge-danger">Fail</span>
										Sorry, there was an error uploading your file.
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
										</button>
										</div>';
          }

          // // Create connection
          // $con=mysqli_connect("localhost","root","","project");
          // // Check connection
          // if (mysqli_connect_errno()){
          //   echo "Failed to connect to MySQL: " . mysqli_connect_error();
          // }



          echo '<div class="row">
            <div class="col-lg-12">
                <div class="card bg-white">
                    <div class="card-body">
                      <table id = "geneData">
                          <center>

                            <thead style="text-align:center;">
                              <tr>
                                  <th>seqid</th>
                                  <th>source</th>
                                  <th>type</th>
                                  <th>start</th>
                                  <th>end</th>
                                  <th>strand</th>
                                  <th>phase</th>
                                  <th>ID</th>
                                  <th>Dbxref</th>
                                  <th>Name</th>
                                  <th>Note</th>
                                  <th>gbkey</th>
                                  <th>gene_biotype</th>
                                  <th>locus_tag</th>
                                  <th>product</th>
                                  <th>protein_id</th>
                                  <th>transl_table</th>
                                  <th>Is_circular</th>
                                  <th>genome</th>
                                  <th>mol_type</th>
                                  <th>old_name</th>
                                  <th>strain</th>
                              </tr>
                          </thead>';

                          $row = 1;


                          if (($handle = fopen($target_file, "r")) !== FALSE) {
                              while (($data = fgetcsv($handle, 1000000, "\t")) !== FALSE) {

                                  $num = count($data);
                                  //echo "<p> $num fields in line $row: <br /></p>\n";//show


                                  //remove ## line in gff file
                                  $re = '~##~';
                                  preg_match_all($re, $data[0], $matches, PREG_SET_ORDER, 0);

                                  // Print the entire match result
                                  // var_dump($matches);


                                  if ($matches == true) {
                                    $row++;
                                    continue;
                                  }
                        echo '<tbody>
                              <tr>';
                              for ($c=0; $c < $num; $c++) {
                                  //echo $data[$c] . "<br />\n";
                                  if ($c == 8) {
                                    break;
                                  }


                                  echo '
                                    <td class="column',$c+1,'">'
                                      ,$data[$c],
                                    '</td>';


                                    if($data[$c] == "region") {
                                      $importSQL5 = "INSERT INTO region (seqid,source,type,start,endPos,score,strand,phase,attributes)
                                                   VALUES('".$data[0]."','".$data[1] ."','".$data[2]."','".$data[3]."','".$data[4]."','".$data[5]."','".$data[6] ."','".$data[7]."','".$data[8]."')";
                                      mysqli_query($con, $importSQL5);
                                      //echo "region here";
                                      //echo $importSQL5 . "<br />\n";




                                      // $re = '~##~';
                                      //
                                      // preg_match_all($re, $data[$c], $matches, PREG_SET_ORDER, 0);
                                      //
                                      // // Print the entire match result
                                      // var_dump($matches);



                                    }


                              }

															if($data[3] == 0 && $data[4] == 0) {
																$importSQL20 = "DELETE FROM gff2 WHERE startPos = 0 and endPos = 0";
																mysqli_query($con, $importSQL20);
																// echo $importSQL20 . "<br />\n";
															}
															// echo "<br />\n";


                              //length of circle
                              if ($data[2] == 'region') {
                                $len =  $data[4];

                                // $_SESSION['len'] = $len;
                                //
                                // echo 	$len;

                                // echo 'len ' ,$len;
                              }



                              // start position

                              if($data[2] == 'gene') {
                                  $gene_start[$row] = $data[3];
                                  // echo 'gene ' ,$gene_start[$row];
                              }

                              elseif ($data[2] == 'CDS') {
                                $CDS_start[$row] = $data[3];
                                // echo 'CDS ' ,$CDS_start[$row];
                              }

                              //--------------------------
                              //stop position
                              if($data[2] == 'gene') {
                                  $gene_stop[$row] = $data[4];
                                  // echo 'gene ' ,$gene_stop[$row];
                              }

                              elseif ($data[2] == 'CDS') {
                                $CDS_stop[$row] = $data[4];
                                // echo 'CDS ' ,$CDS_stop[$row];
                              }

                              if($data[0] == NULL) {
                                $importSQLisnull = "DELETE FROM gff2 where seqid is null";
                                mysqli_query($con, $importSQLisnull);
                              }

                              $importSQL = "INSERT INTO gff2 (seqid,source,ctype,startPos,endPos,score,strand,phase,jobID)
            															 VALUES('".$data[0]."','".$data[1] ."','".$data[2]."','".$data[3]."','".$data[4]."','".$data[5]."','".$data[6] ."','".$data[7]."','".$jobID."')";
            									mysqli_query($con, $importSQL);

															// echo $importSQL;



            									// if($data[3] == 0 && $data[4] == 0) {
            									// 	$importSQL20 = "DELETE FROM gff2 WHERE startPos = 0 and endPos = 0";
            									// 	mysqli_query($con, $importSQL20);
															// 	// echo $importSQL20 . "<br />\n";
            									// }
                              // // echo "<br />\n";

                							//$text = $data[8];
                							$attr = explode(";", $data[8]);
                							$num2 = count($attr);



                							//echo $num2;
                							// print_r ($attr);

                							$data2 = array();

                							for ($i = 0; $i<$num2; $i++) {
                								//echo $attr[$i]."<br>";
                								$attrequal = explode("=", $attr[$i]);
                								//print_r ($attrequal);
                								// echo $attrequal[0]."<br />\n";

                								$data2[$attrequal[0]]=$attrequal[1];

																// echo "$data['locustag']";


																// echo $data[0];


                								// if($attrequal[0] == "ID"){
                								// 	// $importSQL4 = "UPDATE Attribute
                								// 	// 							SET ID = ".$attrequal[1]." WHERE Name = ".$name;
                								// 	//  mysqli_query($con, $importSQL4);
                								// 	//  echo $importSQL4 . "<br />\n";
                								//
                								// 	//echo $attrequal[1]."<br />\n";
                								// }

																if($attrequal[0] == "Name"){
                									$importSQL3 = "INSERT INTO Attribute3 (Name)
                									 			  			VALUES('".$attrequal[1]."')";
                									 mysqli_query($con, $importSQL3);
                									 //$name = array(0=>"name"[1],1=>$attrequal[1]);
                									 //echo $attrequal[1];
                									 //print_r($name);
                									 //echo $importSQL3 . "<br />\n";

                								}

																// if($attrequal[0] == "locus_tag"){
                								// 	$sqlLocus = "INSERT INTO gff2 (locus_tag)
                								// 	 			  			VALUES('".$attrequal[1]."')";
                								// 	 mysqli_query($con, $sqlLocus);
                								// 	 //$name = array(0=>"name"[1],1=>$attrequal[1]);
                								// 	 //echo $attrequal[1];
                								// 	 //print_r($name);
                								// 	 echo $sqlLocus . "<br />\n";
																//
                								// }

																// echo $data[3];

																if($attrequal[0] == "Name"){
                									$sqlName = "UPDATE gff2
																							 SET Name = '".$attrequal[1]."' WHERE ctype = '".$data[2]."' and startPos = '".$data[3]."' and endPos = '".$data[4]."'";
                									 mysqli_query($con, $sqlName);
                									 //$name = array(0=>"name"[1],1=>$attrequal[1]);
                									 //echo $attrequal[1];
                									 //print_r($name);
                									 // echo $sqlName . "<br />\n";

                								}

																if($attrequal[0] == "locus_tag"){
                									$sqlLocus = "UPDATE gff2
																							 SET locus_tag = '".$attrequal[1]."' WHERE Name = '".$data2["Name"]."'";
                									 mysqli_query($con, $sqlLocus);
                									 //$name = array(0=>"name"[1],1=>$attrequal[1]);
                									 //echo $attrequal[1];
                									 // print_r($name);
                									 // echo $sqlLocus . "<br />\n";

                								}

                                	}
                                  echo '
                    								<td>',$data2["ID"],'</td>
                    								<td>',$data2["Dbxref"],'</td>
                    								<td>',$data2["Name"],'</td>
                    								<td>',$data2["Note"],'</td>
                    								<td>',$data2["gbkey"],'</td>
                    								<td>',$data2["gene_biotype"],'</td>
                    								<td>',$data2["locus_tag"],'</td>
                    								<td>',$data2["Parent"],'</td>
                    								<td>',$data2["product"],'</td>
                    								<td>',$data2["protein_id"],'</td>
                    								<td>',$data2["transl_table"],'</td>
                    								<td>',$data2["Is_circular"],'</td>
                    								<td>',$data2["genome"],'</td>
                    								<td>',$data2["mol_type"],'</td>
                    								<td>',$data2["old_name"],'</td>
                    								<td>',$data2["strain"],'</td>';

                    							$importSQL4 = "UPDATE Attribute3
                    														SET ID = '".$data2["ID"]."' WHERE Name = '".$data2["Name"]."'";
                    							 mysqli_query($con, $importSQL4);
                    							 //echo $importSQL4 . "<br />\n";

                    							 $importSQL6 = "UPDATE Attribute3
                    						 								SET Dbxref = '".$data2["Dbxref"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL6);
                    						 	 //echo $importSQL6 . "<br />\n";

                    							 $importSQL7 = "UPDATE Attribute3
                    						 								SET Is_circular = '".$data2["Is_circular"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL7);
                    						 	 //echo $importSQL7 . "<br />\n";

                    							 $importSQL8 = "UPDATE Attribute3
                    						 								SET gbkey = '".$data2["gbkey"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL8);
                    						 	 //echo $importSQL8 . "<br />\n";

                    							 $importSQL9 = "UPDATE Attribute3
                    						 								SET genome = '".$data2["genome"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL9);
                    						 	 //echo $importSQL9 . "<br />\n";

                    							 $importSQL10 = "UPDATE Attribute3
                    						 								SET mol_type = '".$data2["mol_type"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL10);
                    						 	 //echo $importSQL10 . "<br />\n";

                    							 $importSQL11 = "UPDATE Attribute3
                    						 								SET old_name = '".$data2["old-name"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL11);
                    						 	 //echo $importSQL11 . "<br />\n";

                    							 $importSQL12 = "UPDATE Attribute3
                    						 								SET strain = '".$data2["strain"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL12);
                    						 	 //echo $importSQL12 . "<br />\n";

                    							 $importSQL13 = "UPDATE Attribute3
                    						 								SET Note = '".$data2["Note"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL13);
                    						 	 //echo $importSQL13 . "<br />\n";

                    							 $importSQL14 = "UPDATE Attribute3
                    						 								SET locus_tag = '".$data2["locus_tag"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL14);
                    						 	 // echo $importSQL14 . "<br />\n";



                    							 $importSQL15 = "UPDATE Attribute3
                    						 								SET gene_biotype = '".$data2["gene_biotype"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL15);
                    						 	 //echo $importSQL15 . "<br />\n";

                    							 $importSQL16 = "UPDATE Attribute3
                    						 								SET Parent = '".$data2["Parent"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL16);
                    						 	 //echo $importSQL16 . "<br />\n";

                    							 $importSQL17 = "UPDATE Attribute3
                    						 								SET product = '".$data2["product"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL17);
                    						 	 //echo $importSQL17 . "<br />\n";

                    							 $importSQL18 = "UPDATE Attribute3
                    						 								SET protein_id = '".$data2["protein_id"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL18);
                    						 	 //echo $importSQL18 . "<br />\n";

                    							 $importSQL19 = "UPDATE Attribute3
                    						 								SET transl_table = '".$data2["transl_table"]."' WHERE Name = '".$data2["Name"]."'";
                    						 	 mysqli_query($con, $importSQL19);
                    						 	 //echo $importSQL19 . "<br />\n";

                    							// while (($data[8] = fgetcsv($handle, 0, ";")) !== FALSE) {
                    							//
                    							//   // $num2 = count($data[8]);
                    							//   // for ($c2=0; $c2 < $num2; $c2++) {
                    							//   //     //echo $data[8][$c2] . "<br />\n";
                    							// 	//
                    							//   //    echo '
                    							//   //      <td height="30">'
                    							//   //        .$data[8][$c2].
                    							//   //      '</td>';
                    							//   // }
                    							//
                    							//
                    							//   echo '</tr>';
                    							// 	$importSQL2 = "INSERT INTO Attribute (	ID,	Dbxref,	Name,	Note,	gbkey,	gene_biotype,	locus_tag,	Parent,	product,	protein_id,	transl_table)
                    							//  							VALUES('".$data[8][0]."','".$data[8][1] ."','".$data[8][2]."','".$data[8][3]."','".$data[8][4]."','".$data[8][5]."','".$data[8][6] ."','".$data[8][7]."','".$data[8][8]."','".$data[8][9]."','".$data[8][10]."')";
                    							//   mysqli_query($con, $importSQL2);
                    							// 	echo $data[8][0];
                    							//   $row++;
                    							//
                    							// }

                    						 //echo $importSQL2 . "<br />\n";

                    						 // ------------------------------------


                    							$row++;
                    							}
                    							echo '</tr>

                    						</tbody>';

                    							echo '</table>
                    						</div>
                    						</div>
                    						</div>';



                    							// $jobID++;
                    							  $_SESSION["i"] += 1;

                    								// echo $_SESSION["i"];





                    								$importSQLjobid = "INSERT INTO gff2 (jobID)
                    														 VALUES(''".$jobID."')";
                    								mysqli_query($con, $importSQLjobid);

                    							fclose($handle);
                    							}



																	// FASTA file



																	if (($handle = fopen($target_file2, "r")) !== FALSE) {
																		// write '>' to a file for able to regex
																		$txt = '>';
																		$myfile = file_put_contents($target_file2, $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
																		// while (($line = fgets($handle)) !== false) {
																				$message = stream_get_line($handle, 1000000);
																		// echo $message;
																				// echo str_replace('\n','',$line);
															 					// echo $line;
																				// $lineLessNewLine = preg_replace('/\r?\n$/', '', $line);
																				// $lineLessNewLine = str_replace('\r\n', '', $line);
																				// echo $lineLessNewLine;
																				// $line = str_replace("\n", "", $line);
																				// $line = str_replace("\r", "", $line);
																				// $line = trim ($line);
																				// echo $line;
																				$re = '/locus_tag=(\w+)/m';
																	      $re2 = '/(?<=CDS]).+?(?=>)/s';
																				preg_match_all($re, $message, $matches, PREG_SET_ORDER, 0);
																				// for ($i=0; $i < count($matches) ; $i++) {
																				//
																				// 	print_r($matches[$i][1]);
																				// }

																				// if ($matches == '') {
																				// 	echo "emp";
																				// }

																				echo "<br />";

																	      preg_match_all($re2, $message, $matches2, PREG_SET_ORDER, 0);
																				// echo count($matches2);
																				for ($i=0; $i < count($matches) ; $i++) {
																					// print_r($matches[$i][1]);
																					// print_r($matches2[$i]);

																					$importSequence = "UPDATE gff2
				                    																SET sequence = '" . $matches2[$i][0] . "' WHERE locus_tag = '" . $matches[$i][1] . "'";
				                    							mysqli_query($con, $importSequence);
																					// echo $importSequence . "<br />\n";
																				}


																	      // print_r($matches2);
																				// if ($matches2 == null) {
																				// 	echo "emp";
																				// }
																				// echo "<br />";


													 					// }
																		fclose($handle);
																	}

																	// if (($handle = fopen($target_file2, "r")) !== FALSE) {
																	// 		while (($data = fgetcsv($handle, 1000000, "\t")) !== FALSE) {
																	// 			$trimmed = file($target_file2, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
																	// 			print_r($trimmed);
																	// 			$re = '/locus_tag=(\w+)/m';
																	// 			$re2 = '/\w+$/s';
																	// 			preg_match_all($re, $trimmed, $matches, PREG_SET_ORDER, 0);
																	// 			// print_r($matches);
																	//
																	// 			echo "<br />";
																	//
																	// 			preg_match_all($re2, $trimmed, $matches2, PREG_SET_ORDER, 0);
																	// 		  print_r($matches2);
																	//
																	// 		 	echo "<br />";
																	//
																	// 		}
																	// 			fclose($handle);
																	// }



                    							// $_SESSION['len'] = $len;
                    							// // $len = $_SESSION['len[i]'];
                    							// echo 	$len;


                    						?>

                    </div>


                </div>
            </div>



          </div>

          </center>




          <!-- Widgets  -->


              </div>
              <!--/.col-->






            <!-- create job -->
            <div class="row">
              <div class="col-xl-8">


              </div>
            </div>

            </div>

            <div class="clearfix"></div>

            <footer class="site-footer">
                <div class="footer-inner bg-white">
                    <div class="row">
                        <div class="col-sm-6">
                             &copy; 2018, Senior Project
                        </div>
                        <div class="col-sm-6 text-right">
                            Information & Information Technology (ICT), PSU Hatyai Thailand
                        </div>
                    </div>
                </div>
            </footer>


            <!-- end create job -->

            <!-- /.col-md-4 -->





    </div><!-- /#right-panel -->



    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/js/lib/chart-js/Chart.bundle.js"></script>


    <!--Chartist Chart-->
    <script src="assets/js/lib/chartist/chartist.min.js"></script>
    <script src="assets/js/lib/chartist/chartist-plugin-legend.js"></script>


    <script src="assets/js/lib/flot-chart/jquery.flot.js"></script>
    <script src="assets/js/lib/flot-chart/jquery.flot.pie.js"></script>
    <script src="assets/js/lib/flot-chart/jquery.flot.spline.js"></script>


    <script src="assets/weather/js/jquery.simpleWeather.min.js"></script>
    <script src="assets/weather/js/weather-init.js"></script>


    <script src="assets/js/lib/moment/moment.js"></script>
    <script src="assets/calendar/fullcalendar.min.js"></script>
    <script src="assets/calendar/fullcalendar-init.js"></script>
    <script src="assets/js/index1.js"></script>

    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="assets/js/lib/data-table/datatables.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/jszip.min.js"></script>
    <script src="assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="assets/js/lib/data-table/datatables-init.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="assets/js/angular.min.js"></script>


    <script>

    $(document).ready(function() {

      $('#bootstrap-data-table-export').DataTable();
    } );


    </script>




<!-- <div id="container">



</div> -->



</body>
</html>
