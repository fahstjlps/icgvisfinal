<?php
// Start the session
  session_start();
  $_SESSION["i"] = 1;
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Upload File - iCGVis</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <script src="assets/js/index1.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script>

    <link rel="apple-touch-icon" href="images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png">
    <script src="assets/js/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/pe-icon-7-filled.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css"> -->
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

    <style>
    #weatherWidget .currentDesc {
        color: #ffffff!important;
    }
        .traffic-chart {
            min-height: 335px;
        }
        #flotPie1  {
            height: 150px;
        }
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        }

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;
        }

    </style>

</head>
<body ng-app="appCtrlModule" ng-controller="dataCtrl">


  <!-- Left Panel -->
  <aside id="left-panel" class="left-panel">
      <nav class="navbar navbar-expand-sm navbar-default">
          <div id="main-menu" class="main-menu collapse navbar-collapse">
              <ul class="nav navbar-nav">
                  <li>
                      <a href="index.html"><i class="menu-icon fa fa-laptop"></i>Home </a>
                  </li>
                  <li class="menu-title">Overview</li><!-- /.menu-title -->
                  <li class="active">
                      <a href="uploadFile.php"> <i class="menu-icon fa fa-file-text-o"></i>Create Job </a>
                  </li>
                  <li>
                      <a href="job.html"> <i class="menu-icon fa fa-folder-open-o"></i>My Jobs </a>
                  </li>

                  <li class="menu-title">Other</li><!-- /.menu-title -->
                  <li class="menu-item-has-children dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="menu-icon fa  fa-question-circle"></i>Help</a>
                      <ul class="sub-menu children dropdown-menu">
                          <li><i class="menu-icon fa fa-sign-in"></i><a href="index.html">Guide</a></li>
                      </ul>
                  </li>
              </ul>
          </div><!-- /.navbar-collapse -->
      </nav>
  </aside><!-- /#left-panel -->
  <!-- Left Panel -->



  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">

      <!-- Header-->
      <header id="header" class="header">
          <div class="top-left">
              <div class="navbar-header">
                  <!-- <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a> -->
                  <!-- <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
                  <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
              </div>
          </div>
          <div class="top-right">
              <div class="header-menu">
                  <div class="header-left">

                  <div class="user-area dropdown float-right" id="userProfile">
                      <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true"
                          aria-expanded="false">
                          <img class="user-avatar rounded-circle" src="admin.jpg" alt="User Avatar">
                      </a>

                      <div class="user-menu dropdown-menu">

                          <a class="nav-link" href="#" id="signOut"><i class="fa fa-power-off"></i>Logout</a>
                      </div>
                  </div>
              </div>
          </div>
      </header><!-- /header -->
      <!-- Header-->



        <div class="content pb-0">

          <!-- topic -->
            <div class="row">
                <div class="col col-lg-12">
                    <section class="card">
                        <div class="card-body text-secondary">
                          <h4 class="box-title">Upload File</h4>
                        </div>
                    </section>
                </div>
            </div>
          <!-- End Topic -->



          <div class="row">
              <div class="col-lg-6">
                  <section class="card">
                    <div class="card-body">

                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-1">
                                <i class="fa fa-pencil"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib" style="margin: 15px 0 0 0;">
                                    <!-- <div class="stat-text">$<span class="count">23569</span></div> -->
                                    <div class="stat-text">Job Name</div>
                                </div>
                            </div>
                            <div class="row form-group" style="margin: 25px 0 20px 0;">
                                <div class="col col-sm-7"><input type="text" class="form-control" ng-model="jobName"></div>
                            </div>
                        </div>
                    </div>



                  </section>
              </div>


              <div class="col-lg-6">
                  <section class="card">
                    <div class="card-body">

                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2" >
                                <i class="fa fa-paperclip"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib" style="margin: 15px 0 0 0; ">
                                    <!-- <div class="stat-text">$<span class="count">23569</span></div> -->
                                    <div class="stat-text">Upload GFF-3 File</div>
                                </div>
                            </div>
                            <center><div class="main">
                               <div class="sub-main">
                                   <ul class='actions'>
                                     <form target='_blank' action='table.php' method='post' enctype='multipart/form-data'>
                                         <div>
                                            <input type='file' name='fileToUpload' id='fileToUpload' multiple='multiple'>
                                         </div>
                                         <div class='text-left dib' style='margin: 15px 130px 0 0; '>
                                             <div class='stat-text'>Upload FASTA File {{userID}}</div>

                                         </div>
                                         <div>

                                            <input type='file' name='fileToUpload2' id='fileToUpload2' multiple='multiple'>
                                         </div>
                                         <section>
                                            <input type='hidden' name='jobName' value='{{jobName}}'>
                                           <input type='hidden' name='userID' value='{{userID}}'></section>
                                           </br></br><a href='table.php'><button class='btn btn-primary btn-lg' name='action'><span>Submit</span></button></a>
                                       </form>
                                       </ul>
                               <!-- </br></br><a href="table.php"><button class="button-two" name='action'><span>Submit</span></button></a> -->
                                </div>
                             </div></center>

                        </div>
                    </div>



                  </section>
              </div>


          </div>





          <!-- Widgets  -->


              </div>
              <!--/.col-->






            <!-- create job -->
            <div class="row">
              <div class="col-xl-8">


              </div>
            </div>

            </div>

            <div class="clearfix"></div>

            <footer class="site-footer">
                <div class="footer-inner bg-white">
                    <div class="row">
                        <div class="col-sm-6">
                             &copy; 2018, Senior Project
                        </div>
                        <div class="col-sm-6 text-right">
                            Information & Information Technology (ICT), PSU Hatyai Thailand
                        </div>
                    </div>
                </div>
            </footer>


            <!-- end create job -->

            <!-- /.col-md-4 -->





    </div><!-- /#right-panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>

    <script src="assets/js/lib/chart-js/Chart.bundle.js"></script>


    <!--Chartist Chart-->
    <script src="assets/js/lib/chartist/chartist.min.js"></script>
    <script src="assets/js/lib/chartist/chartist-plugin-legend.js"></script>


    <script src="assets/js/lib/flot-chart/jquery.flot.js"></script>
    <script src="assets/js/lib/flot-chart/jquery.flot.pie.js"></script>
    <script src="assets/js/lib/flot-chart/jquery.flot.spline.js"></script>


    <script src="assets/weather/js/jquery.simpleWeather.min.js"></script>
    <script src="assets/weather/js/weather-init.js"></script>


    <script src="assets/js/lib/moment/moment.js"></script>
    <script src="assets/calendar/fullcalendar.min.js"></script>
    <script src="assets/calendar/fullcalendar-init.js"></script>
    <script src="assets/js/index1.js"></script>

    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


    <script src="assets/js/lib/data-table/datatables.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="assets/js/lib/data-table/jszip.min.js"></script>
    <script src="assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="assets/js/lib/data-table/datatables-init.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>




    <script>

    $(document).ready(function() {
      $('#bootstrap-data-table-export').DataTable();
    } );

    var userID = sessionStorage.getItem("userID");
    var app = angular.module('appCtrlModule', []);

    app.controller('dataCtrl', function ($scope, $http, $httpParamSerializerJQLike) {

      $scope.userID = sessionStorage.getItem("userID");

      $http({
        url: 'assets/php/data.php',
        method: 'POST',
        data: $httpParamSerializerJQLike({ 'case': 'readjob', 'userID': userID }),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).then(function (res) {
        { $scope.jobs = res.data }
      }), function errorCallback(ress) {
        console.log(ress)
      }
    })


    </script>




<!-- <div id="container">



</div> -->



</body>
</html>
